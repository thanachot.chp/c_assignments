// รับจำนวนนักเรียนและคะแรรเพื่อหาคะแนนที่มากและน้อยที่สุด
#include <stdio.h>
int main() { 
    
    int numStudents = 0 ; //จำนวนนักเรียน 
    char names[20][32] ; //ชื่อนักเรียน 
    int score[20] ; //คะแนนข้อสอบ  
    int i = 0 ; //ใช้ดำเนินการทำงานการทำซ่ำ
    int maxScore = 0 ; //รับค่าคะแนนที่เยอะ 
    int minScore = 100 ; //รับค่าตะแนนที่น้อย 
    int indexMax ; //คะแนนที่เยอะที่สุดของทุกคน //
    int indexMin ; //คะแนนที่น้อยที่สุดของทุกคน //
    
    //รับจำนวนนักเรียน
    printf("How many racers in the F1 (maximum is 20)?\n");
    scanf("%d",&numStudents);

    //นักเรียนไม่เกิน 20 คน
    if (numStudents<=20) {  
        //รับค่าคะแนนของนักเรียนทุกคน
        for (i=0;i<numStudents;i++) {
            printf("Racer name %d\n",i+1);
            scanf("%s",&names[i]);
            printf("score for Racer %d\n",i+1);
            scanf("%d",&score[i]);
            //หาคะแนนที่เยอะที่สุด
            if (score[i]>maxScore) { 
                maxScore=score[i];
                indexMax=i;
            }
            //หาคะแนนน้อยสุด
            if (score[i]<=minScore) {
                minScore=score[i];            
                indexMin=i;
            }
        }
        //แสดงคะแนนมากและน้อยท่สุด
        printf("%s had the highest score in the competiton %d\n",names[indexMax],maxScore);
        printf("%s had the lowest score in the competiton %d\n",names[indexMin],minScore);
    }

    //นักเรียนเกิน 20 คน
    else {
        printf("There are too many students\n");
    }
         return  0 ;
}   