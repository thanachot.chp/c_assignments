//รับจำนวนสินค้าและคำนวณภาษีในรูปแบบใบเสร็จ
#include <stdio.h>
int main() { 
    
    int numProducts = 0; // จำนวนสินค้า
    double Item[20] ; // ราคาสินค้า 
    float vat[20] ; // จำนวนภาษี
    int i = 0 ; // ใช้ดำเนินการทำงานการทำซ่ำ
    float itemsPrice[20] ; // รวมราคาและภาษีของทุกต่อชิ้น 
    float totalVat = 0 ; // ราคาภาษีทั้งหมด
    float totalPrice = 0 ; // รวมราคาสินค้า 
    float total = 0 ; // รวมราคาและภาษีของทุกสินค้า 

    // รับจำนวนสินค้า
    printf("How many items in your basket?\n");
    scanf("%d",&numProducts); 
    
    // สินค้าเกิน 20 ชิ้นหรือไม่
    if (numProducts<=20) {
        for (i=0;i!=numProducts;i++) {
            printf("Enter price for an item %d\n",i+1);
            scanf("%lf",&Item[i]);
            //คำนวนvatกับราคารวม
            if (Item[i]>=100) {
                vat[i] = (Item[i] * 0.07);
                itemsPrice[i] = (vat[i] + Item[i]);
            }
            else {
                itemsPrice[i] = Item[i];
                vat[i] = 0 ;
            }
        }
    
    //แสดงใบเสร็จ
    printf("Here is your receipt\n");
    printf("price\t\tvat\t\ttotal Totel\n");
    printf("------------------------------------------\n");
    for (i=0;i<numProducts;i++) { 
        totalPrice = totalPrice + Item[i];
        totalVat = totalVat + vat[i];
        total = total + itemsPrice[i];
        printf("%lf\t%f\t%f\n",Item[i],vat[i],itemsPrice[i]); 
    }
    printf("------------------------------------------\n");
    printf("%f\t%f\t%f",totalPrice,totalVat,total);
    }
    // สินค้าเกิน 20 ชื้น
    else {
        printf("The program cannot handle this many items \n");
    }
    return 0 ;
}
