//รับค่ารหัสผ่านและรับจำนวนเงินรายปีเพื่อหาภาษี
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//ฟังชันค์ตรวจสอบตัวอักษรทั้งหมดคือตัวเลข
int checkallDigits (char stringToCheck[]) {

    int inputLen = strlen(stringToCheck); // ความยาวรหัส 
    int  i = 0 ; // ทำให้เงื่อนไขทำซ้ำทำงาน
    int  chkDigit = 0 ; // เก็บค่าของฟังชันค์ isdigit 

    //ตรวจสอบตัวอักษร
    for (i=0;i<inputLen;i++) {
        chkDigit = isdigit(stringToCheck[i]);
        if (chkDigit == 0) {
            break;
        }
    }
    return chkDigit ;
}

//ตรวจสอบความยาวของรหัส
int getTaxID () {
    
    char code[5]; // รหัส
    int codelen = 0; // ความยาวรหัส 
    int  result= 0; //  เก็บค่าของฟังชันค์ strlen 
    int chkdigit = 0; // เก็บค่าของฟังชันค์ checkallDigits 

    // ตรวจสอบตัวอักษร
    while ( chkdigit==0 || codelen!=4 ) {
        printf("Enter your tax ID (4 digits, 0000 to end): ");
        scanf("%s",&code);
        codelen = strlen(code); 
        result = strcmp(code,"0000");  
        chkdigit = checkallDigits(code); 
        
        // ความยาวตัวเลขไม่ถึง 4
        if (codelen<4) {
            printf("Invalid tax ID, be 4 characters long \n");
        }
        // ความยาวตัวเลขเกิน 4
        else if (codelen>4) {
            printf("Invalid tax ID, be 4 characters long \n");
        }
        // ตัวอักษรไม่ใช่ตัวเลขทั้งหมด
        if (chkdigit == 0) {
            printf("Invalid tax ID, must be all digits\n");
        }   
    } 
    return result ;
}

// เก็บค่าเงินและตรวจสอบตัวอักษร
double getIncome () {
    
    char income[20] ; // จำนวนเงิน
    int chkdigit = 1 ; // เก็บค่าของฟังชันค์ checkallDigits 
    double income2 ; // จำนวนเงินที่มีค่าทศนิยม 
    
    // เก็บค่าเงินและตรวจสอบว่าเป็นตัวเลขทั้งหมด
    printf("what is your yearly income in bath? ");
    scanf("%s",&income);  
    chkdigit = checkallDigits(income);
    // ค่าเงินไม่เป็นตัวเลขทั้งหมด
    while (chkdigit == 0) {
        printf("Invalid income, must be all digits \n ");
        printf("what is your yearly income in bath? ");
        scanf("%s",&income);  
        chkdigit = checkallDigits(income);
    } 
    income2 = atof(income); //เปลี่ยน str ให้เป็น double
    return income2 ;
}    

int main() {
    
    int income  = 0 ; // รับรายได้  
    float vat = 0 ; // ภาษี
    float totalVat = 0 ; // รวมภาษี 
    int value = 1 ; // เก็บค่าของฟังชันค์ getTaxID //
    int category[4] = {0,0,0,0}; //รับค่าจำนวนครั้งในการคำนวนภาษีแต่ละcategory 
    int i = 0 ;

    while (1) {
        
        // เรียกใช้ฟังชันค์รับและตรวจสอบรหัส
        value = getTaxID() ;
        // รหัสเป็น 0000
        if (value==0) {
            printf("\ntotal tax revenue collected: %f baht\n",totalVat);
            for (i=0;i<4;i++) {
                printf("Tax catrgory %d count: %d\n",i+1,category[i] );
            }
           return 0 ;
        }
        // รหัสไม่เป็น 0000
        else {
            // เรียกใช้ฟังชันค์รับและตรวจสอบค่าเงิน
            income  = getIncome() ;
            // ค่าเงิน<=50000
            if (income <= 50000) {
                vat = income  * 0.0 ;
                printf("\tyour tax bill is %f baht\n",vat);
                category[0]++;
            }
            // ค่าเงิน<=200000
            else if (income <= 200000) {
                vat = income  * 0.1;
                printf("\tyour tax bill is %f baht\n",vat);
                totalVat = totalVat + vat ;
                category[1]++;
            }
            // ค่าเงิน<=1000000
            else if (income <= 1000000) {
                vat = income  * 0.2;
                printf("\tyour tax bill is %f baht\n",vat);
                totalVat = totalVat + vat ;
                category[2]++;
            }
            // ค่าเงิน<1000000
            else {
                vat = income * 0.3;
                printf("\tyour tax bill is %f baht\n",vat);
                totalVat = totalVat + vat ;
                category[3]++;
            }
        }
    }   
}







