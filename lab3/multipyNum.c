// คำนวณการหารและเก็บจำนวนครั้งที่หารลงตัว
#include <stdio.h>
#include <string.h>
int main() { 
    
    int divided = 0;//ตัวถูกหาร
    int divisor = 0;//ตัวหาร
    int count = 0;//จำนวนครั้งที่หารลงตัว 

    // รับค่าตัวหาร
    printf("Input your number for multiples(between 0-10)?\n");
    scanf("%d",&divided);
    
    // ถ้าตัวเลขของตัวหารไม่อยู่ระหว่าง 0-10 จะไม่เข้า Loop
    if (divided >=1 && divided <=10) {
        // รับค่าตัวถูกหาร
        printf("Input your number to chcek (-1 to stop):\n");
        scanf("%d",&divisor);
   
        // loop สำหรับหารลงตัว ใส่ -1 จะจบ loop
        while (divisor!=-1) {
            // หาลงตัวค่าและเก็บจำนวนครั้งที่หารลงตัว
            if (divisor % divided == 0){
                printf("%d is evenly divisible by %d \n",divisor,divided);
                count++;     
                }
            // หารไม่ลงตัว
            else {
            printf("%d is not evenly divisible by %d \n",divisor,divided);
            } 
            printf("Input your number to chcek (-1 to stop):\n");
            scanf("%d",&divisor);
        }
    // แสดงจำวนวนครั้งที่หารลงตัว
    printf("The number of times that is divisible %d",count);
    }
    else {
        printf("Please enter the nmuber for multiples");
    }
    return 0 ;
}
