//คํานวณราคาห้องของโรงแรม
#include <stdio.h>
int main() { 
    
    int roomNumber = 0; //รับค่าเลขที่ห้อง
    int roomPrice = 0; //ราคาของห้อง
    
    // รับเลขห้อง
    printf("Input your room number\n");
    scanf (" %d",&roomNumber);
        
        // หาราคาจากเลขห้อง
    // 1-5
    if (roomNumber >= 1 && roomNumber <= 5) {
        roomPrice = 100;
    }
    // 6-25
    else if (roomNumber >= 6 && roomNumber <= 25) {
        roomPrice = 150;
    }
    // 26-40
    else if (roomNumber >= 26 && roomNumber <= 40) { 
        roomPrice = 200;
    }
    // เกินห้องที่ 40 และน้อยกว่าห้องที่ 1 
    else {
        printf("Please enter the new room number.\n");
    }
    // แสดงหมายเลขห้องและแสดงราคา
    if (roomNumber > 0 && roomNumber <= 40) {
    printf("Your room number is %d and price is %d us",roomNumber,roomPrice);
    }
    return  0 ;
}

